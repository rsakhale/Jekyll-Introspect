# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-introspect"
  spec.version       = "0.1.1"
  spec.authors       = ["Chaitali Patil"]
  spec.email         = ["cpatil@saiashirwad.com"]

  spec.summary       = "An ultra-minimal landing page template with a big cover photo."
  spec.homepage      = "https://cpatil.gitlab.io/Jekyll-Introspect"
  spec.license       = "MIT"

  spec.files = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^(_(includes|layouts|sass|team|services|data)|(contact|elements|LICENSE|README)((\.(txt|md|markdown|html)|$)))}i)
  end

  spec.add_development_dependency "jekyll", "~> 3.3.0"
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.post_install_message = "Thanks for Installing jekyll-introspect by Sai Ashirwad Informatia & Design by Templated.co!!"
end
